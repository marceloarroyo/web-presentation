// simple markdown parser
// Author: Marcelo Arroyo

// Generate html from markdown text
// If domElementId is defined the html is rendered there, else return html text
function md(text, domElementId) {
    // block identification regular expressions
    let header = /^ *(\#{1,6}) *(.*)$/m;
    let headerAlt = /^([^\n]+)\n([-=]+) *$/m;
    let item = /^ *([-*+] |\d+\. )./;
    let bq = /^ *> /;
    let fence = /^ *[`~]{3,}(.*)\n/;
    let eqtn = /^ *\$\$/;
    let ref = /^ *\[([^\]]+)\]\: *((?:.|\n)+) *$/m;
    // inline regular expressions
    let bold = /(\n| )\*\*([^*]+)\*\*/gm;
    let em = /(\n| )\*([^*]+)\*/gm;
    let math = /(\n| )\$([^\$]+)\$/gm;
    let img = /^ *\!\[([^\]]+)\]\(([^\)]+)\)/gm;
    let url = /\[([^\]]+)\]\[([^\]]+)\]/gm;
    let html = '';

    function inline(txt) {
        return txt.replace(bold, ' <b>$2</b>').
               replace(em, ' <em>$2</em>').
               replace(math, ' <span class="mdmath">$2</span>').
               replace(url, (_,t,r) => `<a href="${r}">${t}</a>`);
    }

    for (let b of text.split('\n\n')) {
        let m;

        // header
        if ( m = b.match(header) ) {
            let l = m[1].length;
            html += `<h${l}>${inline(m[2])}</h${l}>\n`;
            continue;
        }

        // header alt
        if ( m = b.match(headerAlt) ) {
            let l = (m[2][0] == '=') ? 1 : 2;
            html += `<h${l}>${inline(m[1])}</h${l}>\n`;
            continue;
        }

        // item
        if ( m = b.match(item) ) {
            let tag = Number.isInteger((m[1][0])) ? 'ol' : 'ul';
            html += `<${tag}>\n`;
            for (let line of b.split('\n'))
                html += '<li>' + inline(line.replace(m[1],'')) + '</li>\n';
            html += `</${tag}>\n`;
            continue;
        }

        // blockquote
        if ( m = b.match(bq) ) {
            html += '<blockquote>\n' + 
                    inline( b.replace(/(^|\n) *\> /gm, '\n') ).trim() +
                    '\n</blockquote>\n';
            continue;
        }

        // fence code
        if ( m = b.match(fence) ) {
            let code = b.split('\n').slice(1,-1).join('\n');
            html += `<pre><code class="language-${m[1].trim()}">` + 
                    `\n${code}\n` + 
                    `</code></pre>\n`;
            continue;
        }

        // img/video/sound
        if ( m = b.match(img) ) {
            let lhs = m[1].split(':');
            let rhs = m[2].split(' ');
            let id = lhs[0].toLowerCase();
            let src = rhs[0];
            let attrs = rhs[1] || '';
            let mime = src.match(/\.(.+)$/)[1];
            let tags = {
                'svg': 'img', 'png': 'img', 'gif': 'img', 'jpg': 'img',
                'ogv': 'video', 'mp4': 'video', 'mpeg': 'video',
                'ogg': 'audio', 'mp3': 'audio'
            };
            let tag = tags[mime];
            if (tag == 'img')
                html += `<img id="${id}" src="${src}" alt="${rhs}">\n`
            else {
                html += `<${tag} id="${id}" controls>\n` +
                        `<source src="${src}" type="${tag}/${mime}">\n` +
                        `</${tag}>\n`;
            }
            if (lhs[1]) html += `<p class="caption">${m[1]}</p>\n`;
            continue;
        }

        // math equation
        if ( m = b.match(eqtn) ) {
            let eq = b.replace(/\$\$/g, '');
            html += `<div class="mdmath">${eq}</div>\n`
            continue;
        }

        // reference/footnote/sidenote or caption (for previous elements)
        if ( m = b.match(ref) ) {
            console.log('Reference: ' + m[1]);
            let cls = {
                '^': 'footnote',
                '#': 'cite',
                '<': 'leftnote',
                '>': 'rightnote'
            };
            let c = cls[ m[1][0] ] || 'caption';
            let id = (c == 'caption' ? 'caption-' : '') + m[1];
            let content = c == 'caption' ? m[1]+m[2] : m[2];
            html += `<p id="${id}" class="${c}">\n` + inline(content) + '</p>\n';
            continue;
        }

        // paragraph
        html += '<p>\n' + inline(b) + '</p>\n';
    }
    console.log(html);
    if (!domElementId)
        return html.trim();
    else {
        let element = document.getElementById(domElementId);
        element.innerHTML = html;

        // postprocess, step 1: Add id to caption previous element
        let captions = document.getElementsByClassName('caption');
        for (let c of captions) {
            let previous = c.previousElementSibling;
            if (previous)
                previous.id = c.id.replace(/^caption-/, '');
        }

        // postprocess, step 2: Render math
        let mathElements = document.getElementsByClassName('mdmath');
        for (let e of mathElements)
            katex.render(e.innerText, e);
        return true;
    }
}
