# Markdown parsing made simple

Abstract:
This document describe the

Markdown elements
-----------------

- This is an item of level 1
- Here we have an item level 2
  and here we have a paragraph continuation
- Another one

Code (fenced) blocks
-------------------------------------

~~~javascript
function sum(x,y) {
    return x+y;
}
~~~

It is highlighter agnostic. You can use [highlight.js][https://highlightjs.org/]
or [prism][https://prismjs.com/].

We can have $\LaTeX$ math (thanks to katex)
-------------------------------------------

$$\sum_{i=0}^n \frac{\psi_i}{\sqrt{\alpha^i}}$$
